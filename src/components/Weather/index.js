import React, { useState } from 'react';
import { get } from 'axios';

import './index.css';

const Weather = () => {
  const [weatherInfo, setWeatherInfo] = useState();

  async function handleCitySelect(e) {
    e.preventDefault();

    try {
      const response = await get(
        `https://api.weatherapi.com/v1/current.json?key=00b3369b045d4a65b9a72633210408&q=${e.target.value}&aqi=no`
      );
      console.log(response.data);

      setWeatherInfo(response.data.current);
    } catch (err) {
      console.error(err);
    }
  }

  return (
    <div className="container">
      {weatherInfo && (
        <table cellSpacing="0" cellPadding="0">
          <tbody>
            <tr>
              <td>Temperature:</td>
              <td>{weatherInfo.temp_c} °C</td>
            </tr>
            <tr>
              <td>Condition:</td>
              <td>
                <>
                  {weatherInfo.condition.text}{' '}
                  <img
                    src={weatherInfo.condition.icon}
                    className="icon"
                    alt="Shows current weather condition"
                    title="Current weather condition"
                  />
                </>
              </td>
            </tr>
            <tr>
              <td>Wind Speed:</td>
              <td>{weatherInfo.wind_kph} km/h</td>
            </tr>
            <tr>
              <td>Pressure:</td>
              <td>{weatherInfo.pressure_mb} mb</td>
            </tr>
            <tr>
              <td>Humidity:</td>
              <td>{weatherInfo.humidity}</td>
            </tr>
            <tr>
              <td>Visibility:</td>
              <td>{weatherInfo.vis_km} km</td>
            </tr>
          </tbody>
        </table>
      )}

      <select onChange={handleCitySelect}>
        <option value="Rijeka">Rijeka</option>
        <option value="Zagreb">Zagreb</option>
        <option value="Split">Split</option>
        <option value="Rome">Rome</option>
        <option value="Berlin">Berlin</option>
        <option value="Helsinki">Helsinki</option>
        <option value="Istanbul">Istanbul</option>
      </select>
    </div>
  );
};

export default Weather;
